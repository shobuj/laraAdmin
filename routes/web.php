<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//front page

Route::get('/', 'adminController@index');
Route::get('/about', 'adminController@about');


//backend page
Route::get('/dashboard', 'adminController@dashboard');

Route::get('/view', 'adminController@view');

Route::get('/studentadd', 'adminController@studentadd');
Route::get('/studentview', 'adminController@studentview');

Route::get('/teacheradd', 'adminController@teacheradd');
Route::get('/teacherview', 'adminController@teacherview');

Route::get('/chat', 'adminController@chat');


Route::get('/dashboard/categories/index', 'CoategoryController@index');
Route::get('/dashboard/categories/create', 'CoategoryController@create');
