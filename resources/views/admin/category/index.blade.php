@extends('layouts.master')

@section('content')
	<div class="content">
    <div class="row">

        <div class="col-xs-12">
          <h2> All Category List</h2>
      		<div class="box">
              
              <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Serial</th>
                        <th>Category</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>1</td>
                          <td>Life Style</td>
                          <td>Show Edit Delete</td>
                      </tr>
                  </tbody>
                </table>
              </div>
                  <!-- /.box-body -->
            </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
  </div>
@endsection