<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class adminController extends Controller
{
    public function index(){
        return view('front.index');
    }
    public function about(){
        return view('front.about');
        //return 'this is about page';
    }


    public function dashboard(){
    	return view('admin.index');
    }
    public function view(){
    	return view('admin.view');
    }

    public function studentadd(){
    	return view('student.add');
    }

    public function studentview(){
    	return view('student.view');
    }

    public function teacheradd(){
    	return view('teacher.add');
    }

    public function teacherview(){
        return view('teacher.view');
    }

    public function chat(){
    	return view('Chat.chat');
    }



}
