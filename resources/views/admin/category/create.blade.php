@extends('layouts.master')

@section('content')
	<div class="content">
		<div class="row">

        <!-- left column -->
        <div class="col-md-10 col-md-offset-1">

        	<h2>Add Category</h2>
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Category form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="cat">Category</label>
                  <input type="text" class="form-control" name="cat" id="cat" placeholder="Enter Category name">
                </div>
              
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
	</div>
  </div>
</div>
@endsection