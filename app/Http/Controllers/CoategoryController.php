<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CoategoryController extends Controller
{
     public function index(){
     	return  view('admin.category.index');
     }
     public function create(){
     	return  view('admin.category.create');
     }
}
